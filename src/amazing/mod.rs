use rand::prelude::*;
use std::io::{stdin, Error};
use thiserror::Error;
#[derive(Debug, Error)]
pub enum GameError {
    #[error("Parse Error")]
    ParseError(#[from] Error),
}
fn initial_greeting() {
    println!("{} ", format!("{} AMAZING PROGRAM\n", tab_space(25)));
    println!(
        "{} ",
        format!(
            "{} CREATIVE COMPUTING MORRISTOWN, NEW JERSEY\n",
            tab_space(15)
        )
    );
    println!("\n");
    println!("\n");
    println!("FOR EXAMPLE TYPE 10,10 AND PRESS ENTER\n");
    println!("\n");
}

fn tab_space(num: i32) -> String {
    let mut str = String::from("");
    for _ in 0..num {
        str += " "
    }
    str
}

fn main_game() -> Result<(), GameError> {
    loop {
        println!("WHAT ARE YOUR WIDTH & LENGTH");
        let mut input = String::new();
        stdin().read_line(&mut input)?;
        let parsed_input = input
            .trim()
            .split(",")
            .map(|x| x.parse::<i32>().expect("Meaningless Dimensions, Try Again"))
            .collect::<Vec<i32>>();
        let (h, v2) = (parsed_input[0], parsed_input[1]);
        let mut w: Vec<Vec<i32>> = vec![vec![0; v2 as usize]; h as usize];
        let mut v: Vec<Vec<i32>> = vec![vec![0; v2 as usize]; h as usize];
        for i in 0..h as usize {
            for j in 1..v2 as usize {
                w[i][j] = 0;
                v[i][j] = 0;
            }
        }
        println!("\n");
        println!("\n");
        println!("\n");
        println!("\n");
        let mut q = 0;
        let mut z = 0;
        let mut x: i32 = rand::thread_rng().gen_range(1..h);
        for i in 1..h {
            if i == x {
                println!(".  ");
            } else {
                println!(".--");
            }
        }
        println!("\n");
        let mut c = 1;
        w[x as usize][1] = c;
        c += 1;
        let mut r = x;
        let mut s = 1;
        let mut entry = 0;
        loop {
            if entry == 2 {
                while w[r][s] == 0 {
                    if r < h {
                        r += 1;
                    } else if s < v2 {
                        r = 1;
                        s += 1;
                    } else {
                        r = 1;
                        s = 1;
                    }
                }
            }
            if entry == 0 && r - 1 > 0 && w[r - 1][s] == 0 {
                if s - 1 > 0 && w[r][r - 1] == 0 {
                    if r < h && w[r + 1][s] == 0 {
                        x = rand::thread_rng().gen_range(1..=3);
                        if x == 3 {
                            x = 4;
                        } else {
                            x = rand::thread_rng().gen_range(1..=2);
                        }
                    } else if z == 1 {
                        x = rand::thread_rng().gen_range(1..=2);
                    } else {
                        q = 1;
                        x = rand::thread_rng().gen_range(1..=3);
                        if x == 3 {
                            x = 4
                        }
                    }
                } else if r < h && w[r + 1][s] == 0 {
                    if s < v2 {
                        if w[r][s + 1] == 0 {
                            x = rand::thread_rng().gen_range(1..=3);
                        } else {
                            x = rand::thread_rng().gen_range(1..=2);
                        }
                        if x >= 2 {
                            x += 1;
                        } else {
                            q = 1;
                            x = rand::thread_rng().gen_range(1..=3);
                            if x >= 2 {
                                x += 1;
                            }
                        }
                    } else if s < v2 {
                        if w[r][s + 1] == 0 {
                            x = rand::thread_rng().gen_range(1..=2);
                            if x == 2 {
                                x = 4;
                            } else {
                                x = 1;
                            }
                        } else if z == 1 {
                            x = 1;
                        } else {
                            q = 1;
                            x = rand::thread_rng().gen_range(1..=2);
                            if x == 2 {
                                x = 4;
                            }
                        }
                    } else if s - 1 > 0 && w[r][s - 1] == 0 {
                        if r < h && w[r + 1][s] == 0 {
                            if s < v2 {
                                if w[r][s + 1] == 0 {
                                    x = rand::thread_rng().gen_range(1..=4);
                                } else {
                                    x = rand::thread_rng().gen_range(1..=3)
                                }
                            } else if z == 1 {
                                x = rand::thread_rng().gen_range(1..=3);
                            } else {
                                q = 1;
                                x = rand::thread_rng().gen_range(1..=4);
                            }
                        } else if s < v2 {
                            if w[r][s + 1] == 0 {
                                x = rand::thread_rng().gen_range(1..=4);
                                if x == 3 {
                                    x = 4;
                                } else {
                                    x = 2;
                                }
                            } else if z == 1 {
                                x = 2;
                            } else {
                                q = 1;
                                x = rand::thread_rng().gen_range(1..=3);
                                if x == 3 {
                                    x = 4;
                                }
                            }
                        } else if r < h && w[r + 1][s] == 0 {
                            if s < v2 {
                                if w[r][s + 1] == 0 {
                                    x = rand::thread_rng().gen_range(1..=4);
                                } else {
                                    x = 3;
                                }
                            } else if z == 1 {
                                x = 3;
                            } else {
                                q = 1;
                                x = rand::thread_rng().gen_range(1..=4);
                            }
                        } else if s < v2 {
                            if w[r][s + 1] == 0 {
                                x = 4;
                            } else {
                                entry = 2;
                                continue;
                            }
                        } else if z == 1 {
                            entry = 2;
                            continue;
                        }
                    } else if z == 1 {
                        entry = 2;
                        continue;
                    } else {
                        q = 1;
                        x = 4;
                    }
                    if x == 1 { //Left

                    }
                }
            }
        }
        break;
    }
    Ok(())
}

pub fn amazing() {
    initial_greeting();
    main_game().ok();
}
