
use rand::prelude::*;
use std::io::stdin;
fn initial_greeting() {
    println!("{} ", format!("{} ACEY DUCEY CARD GAME\n", tab_space(25)));
    println!(
        "{} ",
        format!(
            "{} CREATIVE COMPUTING MORRISTOWN, NEW JERSEY\n",
            tab_space(15)
        )
    );
    println!("\n");
    println!("\n");
    print!("ACEY-DUCEY IS PLAYED IN THE FOLLOWING MANNER \n");
    println!("THE DEALER (COMPUTER) DEALS TWO CARDS FACE UP\n");
    println!("YOU HAVE AN OPTION TO BET OR NOT BET DEPENDING\n");
    println!("ON WHETHER OR NOT YOU FEEL THE CARD WILL HAVE\n");
    println!("A VALUE BETWEEN THE FIRST TWO.\n");
    println!("IF YOU DO NOT WANT TO BET, INPUT a 0\n");
}

fn tab_space(num: i32) -> String {
    let mut str = String::from("");
    for _ in 0..num {
        str += " "
    }
    str
}

fn show_card(num: u32) {
    match num {
        card if card < 11 => println!("{} \n", card),
        11 => println!("JACK\n"),
        12 => println!("QUEEN\n"),
        13 => println!("KING\n"),
        _ => println!("ACE\n"),
    }
}

fn game_logic(line: &str) {
    let mut q = 100;
    let mut a;
    let mut b;
    let mut bet: u32 = 0;
    loop {
        println!("YOU NOW HAVE {} DOLLARS \n", q);
        println!("\n");
        println!("HERE ARE YOUR NEXT TWO CARDS: \n");

        loop {
            a = rand::thread_rng().gen_range(1..13);
            b = rand::thread_rng().gen_range(1..13);
            if a >= b {
                break;
            }
        }
        show_card(a);
        show_card(b);
        println!("\n");
        loop {
            println!("\n");
            println!("WHAT IS YOUR BET");
            let mut bet_input = String::new();
            stdin().read_line(&mut bet_input).unwrap();
            bet = bet_input.trim_end().parse::<u32>().unwrap();
            if bet > 0 {
                if bet > q {
                    println!("SORRY, MY FRIEND BUT YOU BET TOO MUCH.\n");
                    println!("YOU HAVE ONLY {} DOLLARS LEFT TO BET", q);
                    continue;
                }
                break;
            }
            bet = 0;
            println!("CHICKEN!!\n");
            println!("\n");
            break;
        }
        if bet == 0 {
            break;
        }
        let c = rand::thread_rng().gen_range(1..13);
        show_card(c);
        if c > a && c < b {
            println!("YOU WIN!!!\n");
            q = q + bet;
        } else {
            println!("SORRY, YOU LOSE\n");
            if bet >= q {
                println!("\n");
                println!("\n");
                println!("SORRY, FRIEND, BUT YOU BLEW YOUR WAD\n");
                println!("\n");
                println!("\n");
                println!("TRY AGAIN (YES OR NO)");
                let mut input = String::new();
                stdin().read_line(&mut input).unwrap();
                if input == "YES" {
                    q = 100;
                } else {
                    println!("OK., HOPE YOU HAD FUN!");
                    break;
                }
            } else {
                q = q - bet;
            }
        }
    }
}
pub fn aceyducey() {
    initial_greeting();
    let mut line = String::new();
    loop {
        let b1 = stdin().read_line(&mut line).unwrap();
        game_logic(&line);
    }
}
